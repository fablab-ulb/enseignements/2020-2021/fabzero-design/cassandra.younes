## Objet final

**Le galet noir** est une chaise d'extérieur modulable. Elle peut être employée en tant qu'assise simple et renferme à l'intérieur une paroi qui peut devenir un dossier qui s'encastre dans la chaise. La particularité de cette dernière est notamment de pouvoir se prolonger tel un tiroir qui s'ouvre avec une structure dédoublée afin de devenir une assise pour deux. 

La fonction et la forme de la chaise s'inspirent de la *Garden Egg Chair* de Peter Ghyczy à savoir une chaise de petite envergure qui est transportable, qui résiste à l'extérieur tout en gardant étanche un coussin qui lui prodigue un certain confort et sa forme sculpturale lui donne un aspect oeuvre d'art que l'on contemple.

Elle est réalisée entièrement en bois marin noir qui résiste bien face à l'eau et qui a été cintré afin de garder cet aspect organique et sculptural de l'objet de référence. Fermé, sa petite hauteur le rend peu imposant en forme d'oeuf tronqué mais à l'inverse de l'objet de référence. 

La paroi amovible referme un espace intérieur en dessous de l'assise qui contient le coussin et le protège par extension de la pluie. Il s'accroche a la chaise grâce à des scratchs et se retire pour être rangé à l'abris à l'intérieur de celle-ci.

![Visu](images/dessinsvisu.jpg)

![Photos](images/planche2f.jpg)

### Fichiers

[Chaise sur Fusion](Fichiers/Chaise.f3d)

[Assise et dossier SVG](Fichiers/assiseetdossier.svg)

[Paroi extérieure 1 SVG](Fichiers/paroiexterieur1.svg)

[Paroi extérieure 2 SVG](Fichiers/paroiexterieur2.svg)

[Paroi intérieure SVG](Fichiers/paroiintéririeure.svg)

[Paroi extérieure 1 NC](Fichiers/face1.nc)

[Paroi extérieure 2 NC](Fichiers/paroi1.nc)

[Paroi intérieure NC](Fichiers/paroi2x2.nc)

## Recherches

### Jeudi 1 Octobre 2020

Conférence très intéressante au matin sur le Bic, son historique, son design et son évolution. Victor et André nous ont ensuite parlé de leur vélo et de leur compositions très différentes, cela d'un aspect juridique aussi avec les éléments qui peuvent être remplacés.

En après-midi on est allé au musée pour voir les différents objets et en choisir un que l'on va devoir revisiter. J'ai choisi un fauteuil pliable et transportable que j'ai trouvé très intéressant pour cet aspect, il peut etre utilisé en extérieur et une fois plié il est résistant à l'eau. Son look inspiré de l'époque pop art avec une couleur flashy et brillante dû au plastique lui donne un aspect assez particulier qui contraste avec l'intérieur qui à l'air très chaud et confo. 

On a terminé par se présenter et présenter l'objet un par un.

### Jeudi 8 Octobre 2020

Aujourd'hui il y avait les formations git et Fusion 360 pour le module 1 et en module 3 on a fait un petit exercice pour débuter, on a pris un stylo et on a essayé de comprendre les choix du design. Pour la prochaine séance on s'est réparti plusieurs parties à approfondir, l'historique du stylo, l'influence culturelle, le design, la production ainsi que divers tests.

### Jeudi 15 Octobre 2020

On a continué les recherches sur l'objet choisi.

### Jeudi 22 Octobre 2020

Idem.

## Peter Ghyczy - Garden Egg Chair

![Peter](images/Screen-Shot-2018-02-07-at-11.21.48-940x670.png)

<br><ul><li> **Designer** : Peter Ghyczy 

<br></li><li> **Entreprise** : Reuter Produkt Design / Elastogran, Allemagne 

<br></li><li> **Fonction** : Fauteuil

<br></li><li> **Matériau** : Polyuréthane 

<br></li><li> **Date** : 1968 

<br></li><li> **Poids** : 12,5kg </li>

## Peter Ghyczy

| Portrait | Présentation |
| --- | --- |
|![Peter](images/Peter_Ghyczy2.jpg)|Peter Ghyczy a fuit la Hongrie pendant la révolution hongroise de 1956 pour déménager à Aix-la-Chapelle en Allemagne Ouest où il étudia l’ingénierie et l’architecture. C’est pendant ses études qu’il a développé une fascination pour la fonction plutôt que pour la forme “Je n’aime pas les illusions”. Après avoir terminé ses études, il commença à travailler chez Elastogran / Reuter, entreprise allemande où il dirigea le département design entre 1968 et 1972 en travaillant avec un nouveau matériau, le plastique. Ses premières expérimentations avec le plastique étaient pionnières, il tentait de les mêler à une approche fonctionnaliste notamment avec son produit emblématique, la *Garden Egg Chair*. L’objet fût surtout apprécié pour sa forme mais la motivation première de Ghyczy était pragmatique, avoir un meuble extérieur avec des coussins qui reste sec même lorsqu’il pleut. [Interview Peter Ghyczy RTBF Culture](https://www.rtbf.be/auvio/detail_peter-ghyczy-et-la-garden-egg-chair?id=2316303)|

## Reuter / Elastogran

En 1968, Gottfried Reuter de l'entreprise Reuter / Elastogran sollicite Ghyczy afin de créer un centre de design précisemment pour la promotion industrielle d'un produit, le polyuréthane. Le Groupe Elastogran à Lemförde est l'un des leaders mondiaux des polyuréthanes désormais sous le nom de *Polyuréthanes BASF GmbH* et membre du groupe BASF, groupe chimique allemand le plus grand au monde qui s'étend à travers le monde en Europe, Amérique du Nord et Asie Pacifique et qui couvre un grand nombres d'activités, produits pour l'agriculture, colorants, matières plastiques, produits pharmaceutiques, biotechnologie, pétrochimie de base, engrais, peintures, gaz et pétrole et produits de construction. En travaillant avec les ingénieurs de Reuter ils ont pu obtenir de bons résulats quant à l'épaisseur des coques pour les rendre plus légeres mais résistantes. A l'époque c'était très innovant de pouvoir travailler avec ce genre de matériaux, Peter Ghyczy était donc ravi de pouvoir diriger le département design.

Derrière l'histoire de la production de masse de la chaise se cache également une histoire fascinante sur les relations est-ouest en Allemagne. L'entreprise de Lemförde n'a produit que quelques chaises en testant le nouveau matériau qu'était le polyuréthane. Cependant, l'entreprise a décidé de produire en série la chaise en Allemagne de l'Est car la production y était beaucoup moins chère. Les échanges industriels entre l'Allemagne de l'Ouest capitaliste et l'Allemagne de l'Est socialiste n'ont pas été reconnus. La licence de production de la chaise a été vendue au VEB Synthesewerk Schwarzheide près de la ville de Senftenberg.

## Contexte

### Epoque pop

C’est au **début des années 60**, en Angleterre et aux Etats-Unis que se propage un nouveau mouvement flamboyant, **le design pop**. Principalement révélé à l’aide de l’image colorée d’**Andy Warhol**, il s’inspire de la bande dessinée et se démarque par ses **couleurs vives et ses formes pleines**. Les designs sont travaillés en prenant en compte l’aspect **graphique** de l’affichage et de la publicité, dans une optique de **consommation de masse**. Cela marque une certaine **révolution du goût populaire**.

A travers le design pop une volonté d’**anti conventionnalisme** se ressent par un idéal de design **anti fonctionnaliste** dans plusieurs domaines, la musique, l’attitude, les vêtements. Cet élan est perçu par certains designers comme une renaissance du design. 

La génération visée par cette commercialisation est celle du baby boom, ce design n’est pas un style, c’est un réel **mouvement éphémère et variable**. De nombreuses images de symboles caractérisent cette nouvelle tendance du design qui revendique l’**arbitraire**. Cela s’observe dans la mode aussi avec la minijupe et le blue jean qui incitent un **changement de style et d’attitude**, on ne s’assied plus de la même façon.

C’est l’ère du **jetable**, des objets plein d’astuces et **bon marché**. Certains designers dessinent des robes jetables en papier illustré. Les produits deviennent obsolètes, processus favorisé par la pub et le marketing. Les objets sont conçus pour **séduire et susciter la demande** plus que pour répondre à un besoin. Et cela touche notamment le secteur des meubles qui était considéré comme **durable**, avec l'avènement du carton et du PVC gonflable.

### Le plastique

Les réponses de l’industrie du meuble afin de satisfaire les goûts des enfants du baby-boom et répondre à une production en **grande quantité** se matérialisent en fournissants des produits **peu coûteux, mobiles, légers et colorés**. A l’époque le coût du **pétrole** était moindre et les designers s’orientent vers le **plastique**, principalement l’ABS (plastique rigide),le polyéthylène et d’autres thermoplastiques. Leurs qualités, ils sont **légers, résistants, colorés et brillants**.

La matière première avait beau être bon marché, le **moule lui était coûteux** ce qui pousse la **production de masse** pour rentabiliser le coût du moule et permettre au produit d’avoir un prix abordable. Il s’écoule rapidement et est remplacé par un autre produit différent. C’est la **mode changeante**.

### Aspect pratique

De nouveaux besoins se font ressentir spécialement dans les hôpitaux, les administrations et les hôtels, ce qui crée des changements de design. Les meubles doivent être **transportables facilement, aisément manipulables et bon marché**.

Les concepts de **flexibilité et de modularité** se développent pour combiner plusieurs fonctions. 

### Idée du projet et innovation

![o](images/GHYCZY_atelier3.jpg)

C’est dans ce contexte qu’est née l’idée de base de ce projet, créer un fauteuil qui pourrait être utilisé tant en intérieur qu’en extérieur mais surtout qui pourrait passer la nuit dehors sans être endommagé par la pluie. 

Pour cela, le designer Peter Ghyczy a imaginé un fauteuil qui se déploie et peut être fermé à volonté et même utilisé en tant qu’assise ainsi. Cet objet est très représentatif de l’époque et des visions progressistes et utopiques qui dominaient dans les conceptions contemporaines de part sa forme futuriste ovni, sa portabilité et sa fonction de prélassement informel. 

Son aspect extérieur a aussi été travaillé comme une sculpture d'extérieur en forme d’ovni qui peut rester la tout le temps.

![](images/view.jpg)

## Design

## Matériau / Fabrication

Ghyczy travaille avec l'entreprise Elastogran qui emploie principalement le polyuréthane, un matériau découvert en 1937 et connu pour sa polyvalence, sa resistance et sa durabilité. Son utilisation est très varié et couvre divers domaines, au sein du secteur automobile il est employé de plusieurs manières principalement pour les sièges en tant que mousse souple moulée et pour les ailerons, composés des pares-chocs avants, arrières et latéraux pour sa variété de formes, sa légèreté, sa diversité de finitions et son assemblage facile. Ce matériau profère aux ailerons qui doivent survivre en mileu extérieur une grande resistance au rayonnement solaire, aux températures extrêmes et à la pluie.

Lorsqu'il est versé dans un moule métallique, le polyuréthane se solidifie en une masse poreuse ressemblant à de l'os. Il est peu coûteux, léger, presque incassable et peut prendre pratiquement n'importe quelle forme à l'aide d'un moule. Voici un exemple de [fabrication dans le secteur automobile](https://www.youtube.com/watch?v=mXBGFkaQLes&ab_channel=YongjiaPolyurethane).

![](images/10663503_bukobject.jpg)

Une fois moulé le polyuréthane permet à la Garden Egg Chair de se présenter sous plusieurs aspects avec les différentes couleurs flashy.

![](images/viewjj.jpg)

## Formes et fonctions

La fonction de base de la Garden Egg chair est de pouvoir se prélasser en extérieur ce qui cadre le design avec certaines contraintes. L'un des premiers critères qui entre en jeu lorsqu'on parle d'un fauteuil mis à part les questions techniques c'est bien entendu son confort, le matériau sur lequel on s'assoit est-il confortable? La hauteur ainsi que la position des jambes est-elle adéquate? De même pour la position du dos et de la tête. 

Ghyzcy voulait combiner le confort intérieur que l'on retrouve grâce aux coussins sur les fauteuils avec l'extérieur tout en ayant pas plus qu'à refermer un couvercle pour les protéger des intemperies et éviter de devoir tout débarasser de l'extérieur. 

Comme il s'agit principalement d'un fauteuil à usage externe on ne s'assoit pas de la même manière qu'en intérieur, on a tendance à être plus bas comme pour les tables basses en intérieur, on a les jambes plus tendues et le dos repose en arrière.

![](images/541521.JPG)
Neufert

Même si la forme de base de l'oeuf écrasé n'est pas venue tout de suite, le design de la forme intègre des caractéristiques typiques de l'époque: un look de la conquête spatiale, une forme semblable à un ovni, un plastique laqué de couleur vive, la portabilité et la fonction de détente informelle du siège bas. 

Au départ le design de l'objet ressemblait à une valise pour ensuite évoluer en ce qu'il est aujourd'hui.

![](images/vall.jpg)

![](images/dimensions.jpg)

Cependant l'assise ici est encore plus basse que la norme et le dossier n'est pas énormement incliné. Cela laisse penser que ce fauteuil est plus adapté à des personnes plus petites ou à des enfants pour éviter l'inconfort, même si eux aussi semblent avoir adapté leur manière de s'assoir à l'objet.

![](images/ghyczy_egg_chair_sweden_1985_custom_290x194_06200416.jpg)

Outre le confort, ce fauteuil fait face à d'autres contraintes liées à sa portabilité et le contexte dans lequel il doit survivre. Le matériau utilisé pour la coque résiste à la pluie mais il existe de nombreuses failles et jonctions nécessaires à l'aspect pliable et dépliable de l'objet, ces zones la sont des zones qui risquent de faire rentrer de l'eau à l'intérieur, c'est pour cela qu'elles ont été travaillées minutieusement à l'aide de creux qui ne laissent pas passer l'eau et les jointures ont été travaillées telles les jointures dans l'automobile au niveau des portes.

J'ai souhaité passer par le dessin pour essayer de mieux comprendre les éléments qui composent cet objet qui semble cacher des détails ingénieux.

![](images/schema123.jpg)

![](images/schema456.jpg)

![](images/coupe.jpg)
Tentative coupe à la main.

### Réception du projet

La Garden Egg Chair fût fabriquée pendant deux à trois ans avant l’interruption de la production dûe à la complexité dans le processus de laquage et à l’échec en terme de vente. Le prix était beaucoup trop élevé et malgré le succès et l’engouement autour de cet objet pour son pragmatisme et ses fonctionnalités les ventes ne le reflètent pas.  

Peter Ghyczy quitta l’Allemagne en 1972 et fonda sa propre entreprise aux Pays-Bas. En 2001 la production d’une version améliorée de la Garden Egg Chair fût lancée par l’entreprise avec en plus une gamme de couleurs interchangeables pour le siège et la coque.
Mais ce fût encore un échec au niveau des ventes, à part un achat notable de Karl Lagerfeld. La production fût donc de nouveau interrompue.

"En plus, le moule qui sert à le fabriquer est presque cassé. Il faudrait plus de 150.000 euros pour en faire un nouveau. Cet investissement, je ne veux pas le faire. L'oeuf était notre produit phare, mais, hélas, nous n'en tirons rien au niveau commercial. La production de la réédition s'arrêtera donc définitivement. Celui qui en veut une devra acheter un exemplaire vintage. Il y en a suffisamment." Ghyzcy

### Mercredi 28 Octobre 2020

Première tentative de contact avec l'architecte et designer Peter Ghyczy afin d'avoir plus d'informations concernant le moule et la partie cachée et évidée de l'objet.

![](images/hk.JPG)

En regardant sur le site de Ghyczy, il y avait une liste des différents showrooms qui disposent ses objets aux Pays-Bas, à Londres et en Belgique et parmi eux il y en avait un à Bruxelles, à Uccle. L'après-midi je suis donc allée avec Fatima voir ce showroom chez Dominique Rigo qui est en fait une entreprise de design d'intérieur comprenant une équipe de sept architectes d'intérieur et 3 showrooms de mobilier moderne offrant une grande sélection de mobilier contemporain à Bruxelles.

C'était vraiment chouette de pouvoir sortir du cadre musée où l'on ne peut qu'interagir avec le regard et pouvoir toucher l'objet, sentir la matière et surtout tester ses fonctionnalités.

| Photo 1 | Photo 2 | Photo 3 | Photo 4 |
| --- | --- | --- | --- |
| ![](images/122891270_1239055996429086_4272680523047423903_n.jpg) | ![](images/122879023_423570561982754_3704740616259045419_n.jpg) | ![](images/123073580_419744159011944_8244792645328957752_n.jpg) | ![](images/122880782_366775481204782_4573956231941746623_n.jpg) |
| C'était intéressant de pouvoir observer la Garden Egg Chair dans une autre couleur, en blanc ça fait tout de suite plus sobre et moins flashy mais l'aspect plastique se voit fortement. | J'ai pu tourné l'objet dans tous les sens et j'ai relevé la poignée pour le porter à l'arrière. | Le dessous de l'objet était refermé avec une plaque donc on ne voit pas l'intérieur évidé comme sur certaines photos. | J'ai remarqué qu'à l'arrière au niveau de la jonction entre le couvercle et le siège il y avait un interstice donc l'eau peut s'y introduire. |

| Photo 5 | Photo 6 | Photo 7 | Photo 8 |
| --- | --- | --- | --- |
| ![](images/122860994_407245850282527_2076546293068988801_n.jpg) | ![](images/122873750_3679720475424255_1317973953192678819_n.jpg) | ![](images/122873248_648635746013996_4792492147833968319_n.jpg) | ![](images/122993650_720782651980221_694961520838721605_n.jpg) |
| Je trouve que cet objet fermé a un aspect mystérieux comme une petite boite à bijoux, on a envie de voir ce qui s'y cache. | Le couvercle vient simplement se poser sur la partie basse donc il n'y a pas de résistance pour l'ouvrir. | L'intérieur chaud grâce à la texture du coussin semble réelement en contraste avec l'extérieur d'aspect froid à cause du plastique. | Une fois déployé, il reste de petite taille et fait plus penser à un fauteuil pour enfant. |

| Photo 9 | Photo 10 | Photo 11 | Photo 12 |
| --- | --- | --- | --- |
| ![](images/122977828_366467111076714_1997020549047223542_n.jpg) | ![](images/122907209_274084843911668_1571633078450400018_n.jpg) | ![](images/122893410_683073919078824_6276807897324859054_n.jpg) | ![](images/122907212_272512850772184_4993988008102243007_n.jpg) |
| A l'arrière, au niveau de l'interstice on voit qu'il y a un creux pour que le couvercle puisse tourner mais ça permet aussi de recolter l'eau sans qu'elle touche l'arrière du coussin, il y a trois petits trous surement pour l'écoulement de l'eau qui ressort ensuite par le bas qui est aussi perforé. | En enlevant la partie haute du coussin on peut observer le système métallique qui permet de faire tourner le couvercle et aussi les scratchs qui permettent de maintenir le coussin. | En enlevant la partie basse du coussin on peut voir que le siège s'abaisse à l'arrière, il y a une différence de 15 cm entre l'avant et l'arrière de l'assise. | Petit détail sans trop d'importance, les coussins ont leur petite étiquette signé Ghyczy et ils font 5 cm d'épaisseur, le plastique lui ne fait que 2 cm d'épaisseur. |

| Photo 13 | Photo 14 | Photo 15 | Photo 16 |
| --- | --- | --- | --- |
| ![](images/122891830_3692494757449352_6382374748326451394_n.jpg) | ![](images/122954498_293939598293693_400769276521485536_n.jpg) | ![](images/122892042_727035918159331_6149228743610735305_n.jpg) | ![](images/122904685_2451413398495958_3358953238529536155_n.jpg) |
| J'ai essayé de le soulever et ça a été un échec pour moi, impossible de le soulever à une main et impossible de le tenir à l'autre extrémité par le bas vu que je n'ai pas des bras assez long, de plus comme le couvercle n'est pas vraiment fixé il a tendance à s'ouvrir quand on penche l'objet. | Après l'effort le réconfort, j'ai pu m'assoir les jambes tendues et profiter des qualités des sièges bas. J'ai trouvé le coussin particulièremet confortabe surtout pour la partie basse du corps et la hauteur est adéquate pour les jambes tendues. | Par contre au niveau du dos et de la tête je n'ai pas trouvé l'inclinaison très agréable même s'il ne semble pas y avoir de risque de bascule. Ma tête reposait tout juste sur le haut du coussin du coup je me disais que pour une personne plus grande c'est peut être désagréable parce que le tête ne reposerait plus sur le coussin. | J'ai aussi testé jambes pliées et j'ai trouvé ça limite, pour moi c'était encore assez confortable mais pour une personne plus grande l'angle au niveau des genoux doit être inconfortable. |

| Photo 17 | Photo 18 | Photo 19 | Photo 20 |
| --- | --- | --- | --- |
| ![](images/122873248_2645163855794451_4747303110641798013_n.jpg) | ![](images/122893690_362239221785099_443754275231546192_n.jpg) | ![](images/122906105_689981864961077_6528899834850541567_n.jpg) | ![](images/122921016_717732288826213_4268810344232784986_n.jpg) |
| Pour finir j'ai voulu m'assoir au dessus vu qu'il peut sois disant être employé de la sorte. D'abbord dans un certain sens mais l'objet basculait d'avant en arrière et l'assise n'était pas très stable j'ai donc du écarter les jambes pour plus de stabilité. | Mais le plastique avec sa surface super lisse et laquée eh ben ça glisse! | Dans ce sens l'objet ne basculait plus donc je pouvais me poser avec les jambes peu écartées. | Mais bon ça reste le même matériau de finitions dans ce sens et ça glisse! |

### Jeudi 12 Novembre 2020

Afin de revisiter l'objet j'ai tout d'abbord essayé de comprendre la manière de concevoir de l'architecte Ghyczy. Ce que j'ai pu observer dans la plupart de ses interviews et écrits c'est qu'il se concentre principalement sur la fonctionnalité. Ses oeuvres sont travaillées de manière pragmatique avant d'être esthétique, ce qui est un peu ironique parce que sa notoriété lui est en grande partie due à son design iconique pour la Garden Egg Chair alors que la forme s'est développée de manière secondaire. Ghyczy déplore également qu'il y a beaucoup trop d'objets inutiles qui existent et prône l'innovation, que l'on peut observer dans ses intentions de design et la fonction qui était innovante pour l'époque ainsi que l'emploi du polyuréthane. C'est la fonction transportable, pliable et étanche qui rend cet objet innovant et qui cadre le design avec certaines contraintes, ici en s'inspirant d'une valise pour les 3 fonctions principales. Néanmoins, si les fonctions de cet objet étaient supposées être innovantes, dans la finalité plusieurs aspects ne conviennent pas. 

L'objet n'est pas facilement transportable, la fermeture n'est pas fixée et un coup de vent pourrait ouvrir le couvercle, puis question de confort le dossier n'est pas idéal ainsi que la position des jambes. Pour commencer j'ai donc décider de m'attarder sur les éléments qui pour moi pourraient être améliorés.

Pour des questions de confort, je souhaiterai m'inspirer du système des canapés lits ou d'une boite à outils pour avoir un compartiment en dessous qui allongerait la taille du bas du fauteuil pour pouvoir se prélasser les jambes complétement tendues. Pour le dossier je souhaite aussi m'inspirer des transats pour pouvoir le régler afin qu'il soit confortable pour toutes les dimensions.

| Boite à outils | Canapé 1 | Canapé 2 | Canapé 3 | Transat |
| --- | --- | --- | --- | --- |
| ![](images/boite.jpg) | ![](images/canap1.jpg) | ![](images/canap2.jpg) | ![](images/canap3.jpg) | ![](images/transat.jpg) |

En plus j'aimerais ajouter à la fonction de base un tissu qui se déploit comme un préau en m'inspirant des systèmes de voitures décapotables.

| Voiture | Voiture zoom | 
| --- | --- |
| ![](images/voiture.jpg) | ![](images/decaps.jpg) | 

Le dessous de l'objet étant vide je souhaite y intégrer un compartiment qui s'ouvre pour prolonger la longueur et qui peut contenir du rangement également, rajouter des roues pour la transportabilité avec une poignée qui se tire vers l'exterieur pour plus de facilité, je reviens un peu plus à la forme initiale d'une valise. Et je rajoute certains détails comme une couverture et plus de flexibilité.

| Extension | Coupe | Elevation | 
| --- | --- | --- | 
| ![](images/Compartment.jpg) | ![](images/couped.jpg) | ![](images/cab.jpg) | 

### Jeudi 19 Novembre 2020

La semaine passée, on avait un peu tous essayé d'analyser la manière de concevoir du designer de l'objet qu'on a choisi, mais l'exercice demandé relève plutôt de la réinterprétation de cet objet de nos jours avec l'aide de lieux de type maker-space comme les FabLab.
Je me lance dans l’exercice de réinterprétation d’un objet, à savoir comment on le concevrait et produirait aujourd’hui, comment le rendre plus fonctionnel, comment intégrer les outils disponibles dans un FabLab dans cette réflexion et quels matériaux employer.  

Ayant déjà suivi l'atelier DFS au premier quadrimestre l'an passé, je vois de manière un peu plus claire les matériaux que je peux utiliser. Le but étant de réaliser un modèle échelle 1/1, j'écarte la **Laser** pour les parties structurelles qui necessiteront une découpe plus épaisse qui se fera surement à la **CNC** ou avec la **Shaper** qui me parait très utile pour découper des morceaux d'une très grande longueur en une fois qui ne pourrait pas rentrer dans la CNC disponible et j'écarte aussi **l'impression 3D** pour des questions de structure, la laser et l'imprimante 3D serviront donc uniquement pour le prototypage à échelle réduite ou pour des petits éléments. Les matériaux qui viennent le plus logiquement à l'esprit quand on pense à la CNC et qu'on pense en terme de rigidité, c'est le bois ou le métal (aluminium). Je me suis donc penchée sur ces deux matériaux en imaginant une structure qui pourrait faire office de structure ou alors de moule afin de pouvoir utiliser des matériaux qui nécessitent d'être moulés et qui sont légers et résistants comme le plastique par exemple.

Ces matériaux ont des caractéristiques, des résistances et des contraintes très variées, certains nécessitent d'être pleins et donc plus de matière au mètre cube mais sont plus légers alors que d'autre peuvent être évidés mais plus lourds.
Voici quelques données concernant les masses volumique des matériaux :

[Masse volumique](http://markuspopp.me/tables-des-masses-volumiques-de-diverses-substances/)

Masse volumique :

-**Mdf**: 600-800kg/m3

-**Multiplex**: 600kg/m3

-**Aluminium**: 2700kg/m3

-**Polyuréthane rigide**: 1150kg/m3

Pour la réinterprétation de la Garden Egg Chair de Peter Ghyczy, je me suis basée sur les critères et fonctionnalités qui posaient problèmes pour en concevoir une version améliorée. La transportabilité est le point le plus problématique, l’objet a un poids de 12,5kg et une toute petite poignée, il n’est pas facilement transportable. La morphologie (structure et finitions) ainsi que le matériau employé vont donc être des questions primordiales dans le developpement de cette réinterpétation vu qu'ils influencent fortement le poid de l'objet.

Vu que les matériaux cités plus haut ont des manières de résister structurellement différentes je souhaiterais en faire une comparaison une fois le modèle créé pour voir lequel serait le plus adéquat pour obtenir un bon ratio poids/résistance.

Dans ma reflexion sur la forme, je suis partie de l'objet de base pour le modifier. 

| Rogner | Basculement | Partie non exploitée | Ajustement |
| --- | --- | --- | --- |
| ![](images/des1.jpg) | ![](images/des2.jpg) | ![](images/des3.jpg) | ![](images/des4.jpg) |
| J’ai donc commencé par rogner la forme actuelle, jugeant que le débordement sur les côtés ne sert à rien et alourdi l’objet, les accoudoirs n’ont pas besoin de s’étendre à ce point. | J’ai voulu garder une forme arrondie au niveau du sol contrairement à l’architecte qui a rogné son œuf, afin d’avoir un basculement. | Je trouvais dommage que la garden egg chair soit évidée à l’intérieur mais que l’on ne puisse rien y disposer. Dans mon dessin, j’ai voulu exploiter cet espace évidé dans le but d’en faire un rangement. | Le dossier n’est pas réglable et peut être inconfortable pour certaines morphologies, j’ai donc voulu en imaginer un ajustable. |

| Vue | Rotation | Coupe |
| --- | --- | --- |
| ![](images/des5.jpg) | ![](images/des6.jpg) | ![](images/des7.jpg) |
| Les deux extrémités du fauteuil sont reliées par une structure qui peut etre reférmée si l'espace doit être étanche. A l'arrière une barre relie les deux extrémités et sert de poigne pour porter l'objet. | Pour le système de rotation j'imaginais que le dossier serait fixé aux extrémités grâce une barre rotative, et une autre barre rotative avec des dents serait aussi ratachée au dossier mais tout en pouvant bouger horizontalement afin de pouvoir changer de dents pour modifier l'angle du dossier. | J'ai voulu réduire l'objet à une structure avec certains éléments qui maintiennent tout comme les extrémités un peu comme des roues, et une structure pour le dossier qui se rabat sur la structure du bas. |

| Photo maquette 1/10 (1) | Photo maquette 1/10 (2) | Photo maquette 1/10 (3) | Photo maquette 1/10 (4) | Photo maquette 1/10 (5) | Photo maquette 1/10 (6) |
| --- | --- | --- | --- | --- | --- |
| ![](images/126441196_681766372763270_6148010285423838077_n.jpg) | ![](images/126481703_715268746089402_8839207053070956726_n.jpg) | ![](images/126298694_3569416416461226_3588626162129029005_n.jpg) | ![](images/126250399_369241050831637_5338069860474714284_n.jpg) | ![](images/126327201_429953698408715_3465354582325636311_n.jpg) | ![](images/126457605_3672603506093648_5554085198380120763_n.jpg) |

Liens pratiques:

[Comment plier une tôle](https://fr.wikihow.com/plier-une-t%C3%B4le)
[Comment cintrer du contreplaqué](https://fr.wikihow.com/cintrer-du-contreplaqu%C3%A9)

### Jeudi 26 Novembre 2020

Pour travailler sur la légèreté d'un objet, deux critères sont primordiaux, sa forme qui doit être épurée et le matériaux utilisé qui doit être le plus léger possible tout en étant assez résistant pour la fonction qu'il occupe.

En ce qui concerne le matériau je vais utiliser du bois étant donné que la CNC sera l'outil qui va m'aider à réaliser ce projet. 
Quand on parle de bois plusieurs critères entrent en jeu pour le choix de ce matériau. Les bois sont très variés dans leur caractéristiques, certains sont plus tendres, pus épais, plus colorés. Trois questions majeures se posent: **Quelle est la fonction de l'objet?** (légèreté de transport, robustesse d'assise, décoration), **Quel contexte/climat pour l'objet?** (intérieur, extérieur) et pour finir **Quel aspect aura l'objet?** (clair, foncé, coloré, cerné, épuré).

La qualité et la résistance du bois vont dépendre essentiellement de la vitesse de croissance des essences d’arbres. Les arbres résineux sont généralement moins résistants que les feuillus parce qu'ils poussent plus rapidement.

Les cernes permettent de déterminer l’âge de l’arbre, néanmoins le nombre de cernes ne témoignent pas de la robustesse d’un bois, mais plutôt l’écart qui les sépare, plus les cernes sont rapprochés, plus le bois sera dense. La densité du serrage indique la vitesse de croissance d’un arbre.

L'environnement est bien sur à prendre en compte, un arbre ayant évolué dans un environnement rocailleux bénéficiera d’une absorption de minéraux plus importante et sera bien plus résistant qu’un même arbre ayant grandi en forêt.

Il existe quatre différents types de bois classés en fonction de leur densité du plus léger au plus dur.

![](images/boisess.JPG)

![](images/massebois.JPG)

Le bois est une matière vivante, c’est un matériau hygroscopique, ce qui veut dire qu’il absorbe et dégage naturellement de l’eau afin de trouver l’équilibre dans son environnement, il réagit donc différemment en fonction de son contexte. Un bois qui reste dehors toute l’année n’aura pas la même vie qu’un bois en intérieur.

Pour les bois qui devront survivre en extérieur plusieurs options sont possibles, **opter pour un bois naturellement resistant** qui produit une substance chimique qui le rend insensible aux champignons, **acheter un bois traité chez un fabriquant** ou **traiter le bois sois-même** grâce à du vernis ou de l'huile protectrice.

Le choix de l'esthétique du bois est notamment important vu qu'il contribue à l'aspect final de l'objet.

![](images/boisesth.JPG)

Ayant pour but de travailler l'objet dans la légèreté je me concentre principalement sur des bois légers de **classe A** tels que le **sapin** qui est un bois très tendre qui fonctionne très bien en extérieur également et qui est employé pour du mobilier ou de la menuiserie mais notamment en charpente, le **peuplier** qui est un bois facile à travailler et beaucoup employé pour faire des meubles lattés ou en contreplaqués mais son usage reste principalement pour l'intérieur, le **cèdre** qui est un bois très joli de couleur marron/rougeâtre avec une odeur particulière qui est utilisé pour des meubles de maison mais également pour des réalisations extérieures et en ébénisterie.

Pour la modélisation de l'objet je me suis basée en parallèle sur le [catalogue de Bourguignon Bois](https://bourguignonbois.be/fr/) auprès de qui nous avions passé commande il y a deux ans en archi construite et qui propose une grande variété de bois. Avant de passer commande j'ai souhaité modéliser l'objet pour ensuite faire un calcul rapide pour le poids afin de vérifier la viabilité de cette forme avec un matériau précis, et ensuite tester en maquette pour les quelconques problèmes non évalués.

![](images/commandebois.JPG)

| Lattes | Parois | Couvercle |
| --- | --- | --- |
| ![](images/11.jpg) | ![](images/22.jpg) | ![](images/33.jpg) |
| Pour les lattes je me suis basée sur la section 10mm sur 30mm de lattes en sapin pour leur légèreté, je les ai placé pour avoir les dimensions d'une d'assise conforme, elles seraient vissées à un support placé sur la paroi et vissé à cette dernière. En mesurant les airs sur fusion ou alors simplement en calculant on obtient un poids de 3,1kg pour les 40 lattes. | Pour les parois je me suis basée sur du mdf que j'ai chez moi qui fait 6mm d'épaisseur parce que le mdf a une masse volumique plus ou moins moyenne pour du bois mais l'épaisseur devra peut-être varier pour plus de resistance. En mesurant les airs sur fusion on obtient un poids de 2,7kg pour les 4 parois. | Pour le couvercle je voulais un bois léger, fin et pliable pour qu'il ait une forme courbe, je me suis donc basée sur multiplex de 3,5mm qui serait vissé aux parois. En mesurant les airs sur fusion on obtient un poids de 1,2kg pour le couvercle. Au total pour l'instant les élements de l'objets outre la quincaillerie pèsent 7kg ce qui est encore abordable vu que le modèle va changer. |

| Vue (1) | Vue (2) | Vue (3) |
| --- | --- | --- |
| ![](images/lklk.jpg) | ![](images/jhikjk.jpg) | ![](images/tygujhk.jpg) |

| Photo maquette 1/5 (1) | Photo maquette 1/5 (2) | Photo maquette 1/5 (3) |
| --- | --- | --- |
| ![](images/maq1.jpg) | ![](images/maq2.jpg) | ![](images/maq3.jpg) |
| Avant d'attaquer les tests en bois j'ai voulu passer à une plus grande échelle de maquette que la dernière fois en passant au 1/5eme pour avoir plus de détails et voir ce qui ne fonctionne pas. | Sur cette photo on voit que j'ai rajouté des petits morceaux à l'arrière des parois mais l'objet fermé tient normalement sans mais j'en ai eu besoin par la suite. | A l'arrière il y a une barre en sapin vissée aux parois qui sert de poignée et un chevron qui bloque le couvercle dans sa rotation pour qu'il soit maintenu. |

| Photo maquette 1/5 (4) | Photo maquette 1/5 (5) | Photo maquette 1/5 (6) |
| --- | --- | --- |
| ![](images/maq4.jpg) | ![](images/maq5.jpg) | ![](images/maq6.jpg) |
| Le couvercle s'ouvre et se referme sans difficultés, les lattes du couvercle s'alignent avec la barre de rotation fixée aux parois et autour de laquelle tournent les parois du couvercle. | Malheureusement quand on ouvre le couvercle qui devient incliné à 100 degrés, le dossier est tellement lourd que l'objet bascule trop en arrière ce qui nécessite de retravailler la forme arrondi en bas des parois, j'ai donc rajouté des petits morceaux pour simuler la forme qu'il faudrait combler pour que l'objet ne bascule pas complètement à cause du poids . | Le chevron quant à lui semble maintenir le dossier pour qu'il ne tombe pas même avec de la pression mais il est positionné très près de l'axe de rotation donc ça en fait une zone fragile. |

### Questions en cours

Plusieurs questions sont en suspend dans ma tête à ce jour, **comment épurer encore la forme?**, **entre légèreté et robustesse, les matériaux choisis sont-ils les plus adéquats?**, **comment doser le poids du couvercle par rapport à l'avant (bascule, pieds, contre-poid)?**.

### Idées en vrac

| Pliage et transport | Dédoublement |
| --- | --- |
| ![](images/objetplie.jpg) | ![](images/objetdouble.jpg) | 
| J'aimerai essayer de réduire la taille de l'objet quand il se replie en comparaison à quand il est déployé pour faciliter le transport. | J'ai aussi pensé allonger l'objet pour en faire un canapé basculant de jardin dans l'idée des balancelles à jardin. |

### Jeudi 3 Décembre 2020

Suite à la séance passée, j'ai travaillé sur les questions encore en suspend et auxquelles je peux répondre avant la construction en y intégrant les remarques faites ce jour la.

La question de la forme pour le confort, la légèreté, la robustesse et l'équilibre m'a poussé à poduire plusieurs variantes du modèle de base imaginé en tant que réinterprétation. Le couvercle a notamment fait l'objet d'une discussion, son aspect n'étant pas en adéquation avec le reste de la chaise qui semble légère en claire voie, j'ai aussi tenté de le retravailler.

| Couvercle | Mini dossier | Mini dossier 2 | Mini dossier 3 |
| --- | --- | --- | --- |
| ![](images/doc11.jpg) | ![](images/doc12.jpg) | ![](images/doc13.jpg) | ![](images/doc14.jpg) |
| Tout d'abbord j'ai enlevé la partie étanche pour laisser le couvercle en claire voie également et j'ai arrondi les parois du couvercle pour casser le côté droit. | J'ai également voulu tester un dossier intégré et non dépliable mais qui serait bas et qui dépasserait de la paroi. | Ensuite une version avec les parois qui recouvrent toutes les lattes mais qui prennent trop de place. | Et une version ou les parois reprennent la forme de la disposition des lattes pour l'assise et le dossier. |

| Module simple | Intégrable | Lattes 2.0 | Repli |
| --- | --- | --- | --- |
| ![](images/doc21.jpg) | ![](images/doc22.jpg) | ![](images/doc23.jpg) | ![](images/doc24.jpg) |
| J'ai voulu tester de faire un module qui n'aurait pas des parois qui dépassent pour moins de matériaux mais sans couvercle ou avec un couvercle dépliable. | Ce module pourrait également s'intégrer en tant que tabouret table qui se range à l'intérieur de l'objet. | J'ai testé les lattes dans l'autre sens aussi mais ca prend beaucoup plus de matière. | C'est notamment une facilité pour le repli. |

Prochaine étape------> Construire et Tester.

### Jeudi 10 Décembre 2020

Avant de passer à la construction, je modifie une dernière fois le modèle en fonction des dernières remarques. J'opte pour la version 3 du mini dossier en modifiant la forme du trou qui suit désormais les lattes disposées en fonction de l'assise et qui permettra à l'objet disposé à l'intérieur de basculer également et d'avoir une face plane pour être utilisé en tant que petite table basse ou tabouret. J'opte notamment pour les dimensions minimales afin de réduire le poids et la volumétrie de l'objet en me basant sur ce qui existe et en mesurant par rapport à moi-même et d'autres gabarits de personnes.

| Module intérieur | Module sorti | Module placé |
| --- | --- | --- |
| ![](images/chaiseetmodule.jpg) | ![](images/chaisemodulesorti.jpg) | ![](images/chaiseettable.jpg) |

**Dimensions**:

**Dossier**:

<br></li><li> 35 cm de hauteur

<br></li><li> 40 cm de largeur

<br></li><li> 110 degrés d'inclinaison

<br> **Assise**:

<br></li><li> 40 cm  de longueur

<br></li><li> 40 cm de largeur 

<br></li><li> 35 cm de hauteur à l'arrière et 40 cm de hauteur à l'avant, 8 degrés d'inclinaison

<br> **Poids et matériaux chaise**:

<br></li><li> Parois mdf 1.2 cm: 0.0072 m³ x 600 kg/m³ = 4.3 kg 

<br></li><li> Lattes en sapin 1.8 cm x 4.4 cm: 0.0073 m³ x 450 kg/m³ = 3.3 kg

**Total**: 7.6 kg

<br> **Coût chaise**:

<br></li><li> Parois mdf 1.2 cm: 2 morceaux de 0.3 m² x 7.74 €/m² = 4.65 € 

<br></li><li> Lattes en sapin 1.8 cm x 4.4 cm: 4 lattes de 2.4 m x 3.39 € = 13.56 €

<br></li><li> Equerres à deux vis: 46 x 0.41 € = 18.86 €

<br></li><li> Vis 3.5 mm x 12 mm: (92 vis) 1 paquet de 100 x 4.49 € = 4.49 €

<br></li><li> Clous 1.2 mm x 20 mm: (184 clous) 1 paquet de 300 x 3.19 € = 3.19 €

**Total**: 44.75 €

<br> **Temps de production**: 2h40

<br> **Poids et matériaux module intérieur**:

<br></li><li> Parois mdf 1.2 cm: 0.002 m³ x 600 kg/m³ = 1.2 kg 

<br></li><li> Lattes en sapin 1.8 cm x 4.4 cm: 0.005 m³ x 450 kg/m³ = 2.4 kg

**Total**: 3.6 kg

<br> **Coût module intérieur**:

<br></li><li> Parois mdf 1.2 cm: 2 morceaux de 0.083 m² x 7.74 €/m² = 1.3 € 

<br></li><li> Lattes en sapin 1.8 cm x 4.4 cm: 3 lattes de 2.4 m x 3.39 € = 10.17 €

<br></li><li> Calles en sapin 1.8 cm x 4.4 cm: Chutes de 2cm = 0 €

<br></li><li> Vis 3.5 mm x 25 mm: (102 vis) 1 paquet de 100 x 5.49 € et 1 paquet de 25 x 2.39 € = 7.88 €

**Total**: 19.35 €

Total poids chaise et module: 11.2 kg

Total coût chaise et module: 64.1 €

Pour le dessin de la courbe il y a une règle précise afin de faire en sorte que la chaise bascule bien, c'est à dire pas trop mais pas trop peu non plus. Je me suis référée à ce [site](https://www.anycalculator.com/rockingchaircalculator.html#:~:text=In%20order%20for%20a%20rocking,of%20the%20rocker%20arc%20rails.) pour dessiner la courbe, qui explique comment trouver et dessiner le rayon du cercle qui nous donnera la courbe de l'objet. Pour cela il faut multiplier par pi la hauteur d'assise afin d'obtenir le rayon du cercle. Ce site explique notamment les bienfaits d'une chaise à bascule et nous donne quelques petites anectodes sympathiques.

![](images/formebascule.png)

Une fois le dessin terminé j'ai voulu tester en maquette, malgré que cela ne garantira pas que ça marche en vrai les défauts grossiers seront visibles. 

| Photo maquette 1/5 (1) | Photo maquette 1/5 (2) | Photo maquette 1/5 (3) |
| --- | --- | --- |
| ![](images/1maq1sur5.jpg) | ![](images/2maq1sur5.jpg) | ![](images/3maq1sur5.jpg) |

Comme il n'y avait rien d'alarmant si ce n'est que l'inclinaison du dossier serait à tester avec la bascule, je décide de commencer la construction.

| Préparation du plan de travail | Découpe | Matériaux| Assemblage |
| --- | --- | --- | --- |
| ![](images/plandetravail.jpg) | ![](images/decoupeii.jpg) | ![](images/morceauxx.jpg) | ![](images/assembl.jpg) |
| Pour la préparation du plan de travail, à savoir la prépartation de la Shaper pour découper la forme courbe des parois en mdf 1.2 cm, j'ai compté 10 min, le temps de disposer le Shapertape, de le scanner, de placer le dessin, de changer la fraise, etc. | En ce qui concerne la découpe ça m'a plus ou moins pris 1 heure mais c'est parce que par moment j'ai du adopter des positions assez acrobatiques pour continuer la découpe avant que quelqu'un avec des plus longs bras ne m'aide. J'avais 12 tours a faire au total donc plus ou moins 5 min par tour, mais des fois ça allait plus vite, il y a eu beaucoup de temps perdu à essayer d'avoir des dominos en abondance pour pouvoir continuer à fraiser. | Ensuite il fallait découper les lattes en bois qui font 1.8cm sur 4.4cm à une longueur de 40 cm, pour cela on peut utiliser une scie circulaire, une scie à main ou une scie sauteuse. Pour plus de précision je conseille la scie circulaire, j'ai utilisé les trois et la scie circulaire donne de bon résultats avec des découpes bien droites même si l'épaisseur perdue avec la lame est plus grande, alors que la scie à main donne des découpes en biai et pareil avec la scie sauteuse, mais bien entendu pour quelqu'un avec de l'expérience dans le domaine ça doit aller aussi. Le temps de tracer les longueurs et de découper j'ai mis 45min. | Pour l'assemblage j'ai voulu tester différentes manières, tout d'abbord la facilité avec des équerres mais pour des raison de coût j'ai utilisé des équerres à deux vis donc un seul axe de rotation suite aux conseils du vendeur qui me disait que c'est largement suffisant, ce qui est le cas mais le fait d'avoir un seul axe de rotation plus les découpes pas totalement parfaites des lattes en bois font que les lattes se retournent. J'ai donc rajouté 2 clous en dessous de chaque extrémité de lattes pour qu'elles ne tournent pas mais cette solution semble coûteuse. On peut toujours opter pour des équerres à trois vis. L'assemblage avec le dessin des lattes sur les parois, la préforation des lattes et le vissage m'a pris plus ou moins 45 min. Pour le module intérieur, j'ai prédécoupé des calles avec les chutes de lattes en sapin qui me serviront pour l'assemblage et qui sont gratuites mais nécessitent également 3 vis pour qu'il n'y ait plus qu'un axe de rotation. Cette solution semble plus raisonnable mais j'aimerai essayer de développer un système d'accroches imprimées en 3D comme pour mon projet. |

| Echelle 1/1 (1) | Echelle 1/1 (2) | Echelle 1/1 (3) |
| --- | --- | --- |
| ![](images/1maq1sur1.jpg) | ![](images/2maq1sur1.jpg) | ![](images/3maq1sur1.jpg) |

Vous trouverez ici le lien d'une video de ma [rocking chair](https://www.youtube.com/watch?v=IVpQ60d_QtY&ab_channel=CassandraYounes) qui bascule ainsi que le lien d'une [video](https://www.youtube.com/watch?v=iadBtyrIA5s&ab_channel=CassandraYounes) ou je l'essaye.

| Photo (1) | Photo (2) | Photo (3) | Photo (4) |
| --- | --- | --- | --- |
| ![](images/basculer.jpg) | ![](images/asiied.jpg) | ![](images/transport1.jpg) | ![](images/transport2.jpg) |
| La chaise bascule très bien, on a même un peu peur ou le vertige même si elle ne balance pas en arrière. Cela est en partie du au dossier qui est plus court et donc on plus peur de basculer vu que l'on a pas le haut du dos soutenu. Peut-être devrais je tenter de faire un dossier plus long qui pourrait  s'étendre. | Malgré le fait que les parois dépassent de l'assise, elles le font de peu et on peut donc s'assoire différement dans la chaise en fonction de ce que l'on trouve confortable. |  Pour transporter l'objet, les deux trous servent à prendre l'objet par la. Même avec le module à l'intérieur, la chaise reste saisissable par la et de la sorte. | On peut également le porter ainsi ou alors même différement, l'objet comporte pas mal de prises pour le tenir. |

Tests couleurs:

| Couleur (1) | Couleur (2) | Couleur (3) |
| --- | --- | --- |
| ![](images/couleur1.jpg) | ![](images/couleur2.jpg) | ![](images/couleur3.jpg) |

| Couleur (4) | Couleur (5) | 
| --- | --- |
| ![](images/couleur4.jpg) | ![](images/couleur5.jpg) |

### Suite correction du Jeudi 10 Décembre 2020

Après les remarques énoncées lors de la dernière correnction, j'ai décidé de m'inspirer de la forme du module prévu pour l'intérieur de l'objet précédent.

| Photo (1) | Photo (2) |
| --- | --- |
| ![](images/photo1mod.jpg) | ![](images/photo2mod.jpg) |

Ce qui pose problème et nécessite d'être retravaillé est la volumétrie qui doit être affinée pour le transport mais aussi pour rester dans le thème de l'aspect visuel de l'objet à réintérpreter qui devient un objet discret de décoration lorsqu'il est fermé et l'assemblage des différents matériaux qui sort de l'aspect sculptural de l'objet de référence. 
Pour ce faire, j'ai opté pour deux modèles, l'un travaillé en tant que bloc plein deployable et l'autre en tant que structure évidée modulable.

### Premier modèle

| Forme et dimensions | Fermé | Ouvert |
| --- | --- | --- |
| ![](images/facemod.jpg) | ![](images/moduleferme1.jpg) | ![](images/moduleouvert1.jpg) |
| Pour la forme j'ai créé un volume de 35 cm de haut qui a une bascule et qui serait plat du côté haut pour former une assise. J'ai ensuite évidé ce volume pour l'alléger afin qu'il devienne la paroi de ma chaise. | Afin de travailler l'objet comme une masse sculpturale, l'objet est entièrement recouvert d'un seul panneau en bois courbé. Sa forme fermée permet de l'employer en tant qu'assise également, ou d'y disposer des choses. | La courbure du bois se fait grâce à des stries faites dans le panneau, mais les expériences montrent que le bois courbé est résistant lorsqu'il est collé dans sa forme courbe mais que lorsqu'il est souvent plié et déplié cela le fragilise ce qui pourrait être le cas de la jonction entre l'assise et le dossier. |

| Stries | Mesures | Maintient dossier |
| --- | --- | --- |
| ![](images/stries.jpg) | ![](images/courbmdf.jpg) | ![](images/139956004_440550900690972_1127786378526168687_n.jpg) |
| Afin de courber le bois il doit être évidé grâce à des stries, les découpes doivent se faire du côté du panneau qui sera à l'intérieur de la courbe. Le bois doit ensuite être cintré, collé et maintenu pendant des heures afin de garder la forme courbe. [Cintrer du mdf](https://www.youtube.com/watch?v=Jep3IGzRAcY&ab_channel=HatlessChimp) | Pour calculer où et à quelle profondeur strier le bois j'ai utilisé un fichier excel disponible qui fait le calcul avec des mesures encodées telles que le rayon de la courbe et l'angle de courbure. | Afin de maintenir le dossier deux barres sont fixées aux parois mais tournent afin de se positionner dans des creux présents sur l'arrière du dossier. |

| Détails stries | Maintient dossier |
| --- | --- |
| ![](images/DETAILSSTRIES.jpg) | ![](images/premierass.jpg) |

| Maquette 1/6 fermée (1) | Maquette 1/6 fermée (2) |
| --- | --- |
| ![](images/taq11.jpg) | ![](images/taq12.jpg) | 
| Pour tester les épaisseurs de panneaux j'ai utilisé 3 faces de carton gris 1 mm pour obtenir 3 mm qui représente des parois de 18 mm à l'echelle 1/1 et pour commencer 1 face pour le contour de l'objet mais cela était trop peu résistant donc j'ai du doubler l'épaisseur qui correspond à du 12 mm à l'echelle 1/1. Le panneau qui recouvre le tout serait vissé aux parois mais j'ai tenté par la suite de retravailler également l'assemblage. | Pour les parois j'ai décidé de faire d'autre tests pour y intégrer une poignée et un design moins banal. |

| Maquette 1/6 ouverte (1) | Maquette 1/6 ouverte (2) | Maquette 1/6 ouverte (3) |
| --- | --- | --- |
| ![](images/taq21.jpg) | ![](images/taq22.jpg) | ![](images/taq23.jpg) |

Pour assembler cet objet, il faut commencer par découper à l'aide de machines telles que la CNC ou la Shaper les morceaux dessinés, de réaliser les stries afin de courber le bois en y ajoutant de la colle pour fixer le tout, ensuite il faut visser le panneau aux parois et maintenir le panneau courbé dans sa forme à l'aide de sangles pendant 2 jours afin que le bois prenne forme. Les éléments d'appui pour le dossier sont à découper et assembler à l'aide de boulons et tiges filetées. Ce modèle pèse un peu plus de 9kg donc j'ai décidé de le revoir différement dans les épaisseurs de matériaux.  

### Premier modèle retravaillé

| Fermé | Ouvert | Maintient dossier |
| --- | --- | --- |
| ![](images/trop.jpg) | ![](images/tropi.jpg) | ![](images/140020590_1116629515439919_2275678259933134420_n.jpg) |
| J'ai voulu modifier l'assemblage des parois et du panneau en créant des emboitements. | Ces emboitements seraient aussi utilisés pour la jonction entre l'assise et le dossier afin de ne plus avoir une courbe fragile. Le système de fermeture arrière serait aussi en emboitement. | Pour le maintient du dossier cest le même système que précédement mais cette fois les barres sont fixées à une paroi intérieure qui a été placée afin de renforcer la structure pour enlever de l'épaisseur au panneau de l'assise. |

| Fermeture | Maintient dossier |
| --- | --- |
| ![](images/fermetire.jpg) | ![](images/deuxiemeass.jpg) |

| Maquette 1/6 fermée (1) |
| --- |
| ![](images/phtmaq1.jpg) |
| Avec cette version l'épaisseur du panneau est réduite mais le modèle est beaucoup plus résistant du fait qu'il y a un renforcement au milieu. |

| Maquette 1/6 ouverte (1) | Maquette 1/6 ouverte (2) | Maquette 1/6 ouverte (3) |
| --- | --- | --- |
| ![](images/phtmaq2.jpg) | ![](images/phtmaq3.jpg) | ![](images/phtmaq4.jpg) |

Pour assembler cet objet, il faut commencer par découper à l'aide de machines telles que la CNC ou la Shaper les morceaux dessinés, de réaliser les stries afin de courber le bois en y ajoutant de la colle pour fixer le tout, ensuite le panneau et les parois s'assemblent entre eux à l'aide d'emboitements faits par la découpe et maintenir le panneau courbé dans sa forme à l'aide de sangles pendant 2 jours afin que le bois prenne forme. Les éléments d'appui pour le dossier sont à découper et assembler à l'aide de boulons et tiges filetées. Ce modèle pèse moins de 7,5 kg.

### Deuxième modèle

| Photo (1) | Photo (2) | Photo (3) |
| --- | --- | --- |
| ![](images/depart.jpg) | ![](images/depart2.jpg) | ![](images/suite.jpg) |

| Photo (4) | Photo (5) | Photo (6) |
| --- | --- | --- |
| ![](images/suite2.jpg) | ![](images/suite3.jpg) | ![](images/suite4.jpg) |

### Suite au pré-jury

Après le pré-jury j'ai décidé de partir sur l'idée du premier modèle retravaillé à savoir un objet travaillé de manière plus sculpturale avec un panneau courbé. Je me suis questionnée sur la manière dont l'objet allait s'ouvrir ainsi que ses dimensions. J'ai finalement décidé de réduire la longueur de la chaise mais de dédoubler la structure afin qu'elle puisse s'étendre en longueur ou rester simple au choix. Cela dans l'optique de réduire ses dimensions lorsqu'elle n'est pas employée pour qu'elle prenne moins de place.

L'intérieur de la chaise sert de rangement pour le coussin et autre, pour rendre cette zone étanche le bois employé est un bois marin. Les parois extérieures de la chaise sont percées et il y a un jeu de couleur avec les parois intérieures qui sont pleines mais gravées au niveau du percement des parois extérieures créant ainsi une superposition de couleur noir sur brun. 

| Ouverture | Vue 1 maquette 1/4 | Vue 2 maquette 1/4 | Vue 3 maquette 1/4 |
| --- | --- | --- | --- |
| ![](images/trot.jpg) | ![](images/maq1final.jpg) | ![](images/maq2final.jpg) | ![](images/maq3final.jpg) |

Le dossier et l'assise sont superposés l'un sur l'autre et reliés grâce à des charnières ce qui permet au dossier de se rabattre complètement afin de devenir une assise pour la partie allongée. 

Afin de maintenir le dossier à l'angle souhaité il pourrait être maintenu avec des cordes élastisques qui feraient tanguer le dossier mais qui émettent donc une résistance à l'ouverture si ces dernières veulent être bloquées avec le poids d'un homme à un certain moment. Des cordes retractables sont donc préférables.

Pour tester les différents angles de dossier qui peuvent être agréables et les distances des cordes qu'il faut j'ai découpé deux morceaux représentants une assise et un dossier afin de voir directement ce qui est le plus accomodant.

| Dossier-assise fermé | Dossier-assise ouvert | 
| --- | --- |
| ![](images/test2plch.jpg) | ![](images/test1plch.jpg) |

En parallèle j'ai tenté de courber du mdf grâce à la technique du kerf bending qui consiste à strier dans le bois afin d'enlever de la matière pour que le bois puisse plier. Le bois doit être maintenu pendant 24 h avec des sangles afin de garder la forme voulu. Je me suis basée sur les dimensions de la poubelle chez moi parce que ça me paraissait être l'objet le plus facilement sanglable. Le mdf plie assez facilement et prend tout de suite la courbe calculée.

| Calcul stries | Stries et colle | Maintien sangle | Résultat |
| --- | --- | --- | --- |
| ![](images/courbimdf.JPG) | ![](images/gfgfgfgss.jpg) | ![](images/tgyhujikklsd.jpg) | ![](images/141011985_402629607502753_9053529995833750228_n.jpg) |

### Suite dernière correction

Après la dernière correction j'ai changé une ultime fois les dimensions de la chaise ainsi que le design des parois.

La question du dossier était toujours problématique, en reliant ce dernier à l'assise avec des charnières on réussi à obtenir deux positions sur trois voulues pour ce dossier (rabattu, déployé) mais pas fixé à l'angle voulu sans employer d'autre éléments comme des cordes. Avec une assise coulissante c'est le même problème. J'ai donc opté pour un dossier qui serait amovible et rangé dans l'objet. Les parois ont donc été modifiées afin d'y intégrer un percement qui servirait de rangement pour le dossier. Le dossier serait donc une paroi amovible qui se range dans un trou prévu pour et pourrait s'emboiter dans les parois intérieures qui sont percées à cet effet ou simplement être déposé pour devenir une autre assise lorsque la chaise s'ouvre.

![Visu](images/dessinsvisu.jpg)

![](images/dessinsparois.jpg)

| Calcul stries 1 | Mesures | 
| --- | --- |
| ![](images/striespt.JPG) | ![](images/cerclepetit.jpg) |

| Calcul stries 2 | Mesures | 
| --- | --- |
| ![](images/striesgr.JPG) | ![](images/cerclegrand.jpg) |

## Découpes et assemblages

### Découpes

**Outils nécessaires** : CNC / Shaper, Scie circulaire / Scie à table, Foreuse.

La première étape est d'acheter le bois pour ensuite le découper à l'aide des machines. Une fois le bois prédécoupé en plus petits morceaux afin qu'il rentre dans la CNC et en calculant pour avoir le moins de chutes possible on peut commencer la découpe des parois. Ces dernières vont être assemblées à la couverture de la chaise et pour cela un assemblage en tenon-mortaise peut être une solution pour rigidifier le tout qui sera collé et sanglé, il faut donc forer dans l'épaisseur de la paroi pour y rajouter des tourillons, étape à répéter dans la couverture également.

| Achat Bois | Découpes parois CNC | Rajout tourillons |
| --- | --- | --- |
| ![](images/140187675_409070180178074_1862610394841632737_n.jpg) | ![](images/140069193_1189382741463932_4993357372964227817_n.jpg) | ![](images/qsdfghyujiko.jpg) |

Le bois utilisé pour le prototype final est un bois marin résistant à l'eau, un panneau de coffrage ou multiplex bakelisé qui n'a pas les mêmes propriétés que le mdf, il est donc nécessaire de faire des tests pour les courbures avec le matériau employé. De plus il faut régler la hauteur de la lame pour ne laisser qu'une fine couche d'épaisseur restante au bois.

| Tests découpes stries | Test courbure 1 | Test courbure 2 |
| --- | --- | --- |
| ![](images/140083581_3438922429569996_4290642745103263444_n.jpg) | ![](images/140814818_461195925286326_2601147201432292735_n.jpg) | ![](images/140610654_846626512796369_6483896674631098846_n.jpg) |

Pour commencer j'ai réalisé le nombre de stries calculées mais j'ai par la suite réalisé qu'elles n'étaient pas suffisantes et que le bois craquait, j'en ai donc profité pour en faire plus sur le morceaux d'en face qui est le même en miroir. Les stries étaient un peu trop abondantes a la fin et ne se referment pas complètement. Par la suite il faut de même forer afin de rajouter des tourillons.

| Découpe stries côté 1 | Découpe stries côté 2 | Rajout tourillons |
| --- | --- | --- |
| ![](images/140092980_757040534917946_1826680391797320651_n.jpg) | ![](images/sdrtyhujkillkj.jpg) | ![](images/140645691_411222146864084_4453965358343298529_n.jpg) |

### Assemblages

**Outils nécessaires** : Sangles, Serre-joints, Marteau.

Un espace est prévu pour receuillir le coussin en dessous de l'assise et pour isoler cette zone de la pluie qui peut s'infiltrer dans les percements de la couverture, des morceaux ont été placés et assemblés grâce à des tourillons également. Ces éléments facilitent notamment l'assemblage des parois.

| Tiroir | Assemblage parois | 
| --- | --- |
| ![](images/140149994_747984132498075_2484279116544355492_n.jpg) | ![](images/140583073_210611854032090_8032552852678627157_n.jpg) |

Afin que le bois garde une forme courbe, il faut appliquer de la colle dans les stries et maintenir le morceaux de bois courbé à l'aide de sangles et de serre-joints pendant 24 h. L'assemblage des tourillons étant en serrage, l'emploi d'un marteau (doucement!) est nécessaire pour que les morceaux s'emboitent parfaitement. Des rails en bois sont fixés aux parois intérieures et les font coulisser aux parois extérieures.

| Maintient sangle | Maintient sangle | Rails |
| --- | --- | --- |
| ![](images/140081942_408947540318342_5689801933909390286_n.jpg) | ![](images/140111284_239140421072761_168991609745704640_n.jpg) | ![](images/140326192_832320667613871_3568194872354297991_n.jpg) |

| Objet fermé 1 | Objet fermé 2 | Objet ouvert 3 |
| --- | --- | --- |
| ![](images/prototypefinal1.jpg) | ![](images/prototypefinal2.jpg) | ![](images/prototypefinal3.jpg) |

| Objet ouvert 4 | Objet ouvert 5 | Objet ouvert 6 | Objet ouvert 7 |
| --- | --- | --- | --- |
| ![](images/prototypefinal4.jpg) | ![](images/prototypefinal5.jpg) | ![](images/prototypefinal6.jpg) | ![](images/prototypefinal7.jpg) |

**Poids Objet** : 8.5 kg

**Coût Objet** :  
                 
                 - Bois : 30 €

                 - Colle à bois : 1.5 €

                 - Tourillons : 2 €

                 - Scratch : 0.5 €

                 - Coussin : variable

                 - **Total**: 35 €
