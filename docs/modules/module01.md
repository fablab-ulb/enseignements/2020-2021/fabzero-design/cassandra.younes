## Uni Sign Pen 

## Design et fabrication

| | |
| --- | --- |
|![pen1](../images/docs_images_pen1.jpg)| 1. La mine est circulaire pour que l’on puisse écrire parfaitement depuis n’importe quel angle et elle se termine en pointe pour plus de précisions. Cette mine est absorbante, ce qui permet de pouvoir écrire sous n’importe quel angle. Même retourné, le marqueur continuera de fonctionner. <br> 2. Cette partie est la plus petite du stylo, d’une façon qu'elle bloque la mine et elle ne bouge pas en écrivant ou en dessinant.  <br> 3. Pour que le capuchon se ferme bien sûr la mine, cette partie est plus large pour ne pas avoir de l’air au niveau de la mine en fermant le capuchon. <br> 4. En écrivant et avoir un certain contrôle au mouvement du stylo, cette surface est faite pour bloquer les doigts à ne pas descendre encore plus bas. <br> 5. La forme du marqueur est circulaire pour une meilleure prise en main. <br> 6. Sur le marqueur, un léger renfoncement dans son diamètre permet de bloquer le capuchon afin que celui-ci n’écrase pas la mine <br> 7. La forme du stylo s’agrandit jusqu’à ce point pour que le capuchon quand on le met derrière, il s’attache bien au stylo et se détache pas en écrivant. <br> 8. Sa longueur est tel qu’il est confortable de s’en servir. Plus court, il ne tiendrait pas bien dans la main et, plus long, il déséquilibrerait celle-ci. <br> 9. Le stylo a un petit capuchon en arrière pour donner l’accès au tube qui contient l’encre, et on aura plus de facilité à le changer |
|![pen2](../images/docs_images_pen2.jpg)| 10. Le capuchon s’arrête à ce point, pour que la fin de stylo n’agrandit pas la largeur du capuchon qui serre à protéger la mine.   <br> 11. Tout est calculé d’une façon que le stylo ouvert avec le capuchon, n’aura pas un poids ou une longueur désagréable en écrivant.|
|![Pen3](../images/docs_images_pen3.jpg)| 12. Le capuchon du marqueur a des trous au sommet afin de permettre une entré d’air lorsque l’on ouvre le marqueur. <br> 13. Pour que la mine ne sèche pas, le capuchon possède une partie qui ferme et englobe la mine pour qui n'y aura pas d’accès d’air. <br> 14. Le capuchon possède également un genre de crochet qui permet d’accrocher le style à une poche, par exemple. Ce crochet se trouve sur le capuchon afin que l’on ne puisse pas l’accrocher sans que le marqueur soit fermé. Ceci permet d’éviter de se salir. <br> 15. La largeur du capuchon est égale à la largeur du stylo pour une fermeture complète. Pour que le capuchon ne s’ouvre pas en cas de déplacement.|
 
## Influence culturelle

Le Japon porte un très grand intérêt à l’écriture depuis le 5 ème siècle lors de son développement. Celle-ci se composant d’idéogrammes en partie inspirés de Chine et de deux types d’écritures propres au Japon, elle est intéressante d’un point de vue de l’art qui se cache derrière la représentation ainsi que la signification des symboles. 

Au début, plus pratiquée par les nobles et ensuite par les samouraïs, la représentation de ces symboles, la calligraphie japonaise qui dans sa traduction *shodo* signifiant “la voie de l’écriture” renvoie à la religion bouddhiste et à un aspect spirituel de l’art d’écrire à la main qui représente davantage un art de vivre qui maintient l'esprit et le corps qu’un art purement esthétique. (Certains moines l’utilisent pour de la méditation)

Aujourd’hui cet art est intégré dès le plus jeune âge dans les écoles japonaises, et malgré la difficulté de cet apprentissage, le taux d'illettrisme au Japon est infiniment peu élevé et c’est spécifiquement l’emploi d’outils manuels qui justifierait cela selon les puristes japonais, le passage du dessin à la main apporte beaucoup à la compréhension des symboles.

Pour cet art, 4 outils primordiaux sont nécessaires (4 trésors du calligraphe):

![influence culturelle](../images/docs_images_influence_culturelle_Sign_Pen.png)

La préparation de l’encre est une étape nécessaire à la préparation de l’esprit, néanmoins de nos jours, l’emploi de stylo ou du feutre devient de plus en plus répandu pour la calligraphie. 

Ces outils représentent un format portable plus compact de plusieurs éléments, ils peuvent être transportés dans une poche et représentent un gain de temps de préparation vu qu’ils sont prêts à l’emploi directement.

![influence culturelle 2](../images/docs_images_influence_culturelle_2.png)

Outre l’art de la calligraphie, les mangas représentent un secteur très prisé notamment à l’étranger, la pratique de cet art nécessite l’emploi de divers instruments d’écriture tel que les feutres. Le Japon excelle dans les stylos et feutres avec une spécialisation dans la pointe fine permettant aussi divers épaisseurs et arrondis.

Le sign pen semble reprendre les caractéristiques de base nécessaire à la calligraphie japonaise, de l’encre et de l’eau. Cela permet de rendre l’encre résistante à l’eau et donc au temps et donc inaltérable. Néanmoins elle ne possède pas une brosse flexible ce qui lui permet donc de créer peu de variantes dans les épaisseurs mais celle-ci ne laisse pas l’encre transpercer le papier.

## Evolution du Sign Pen

Très répandue au Japon, le Sign Pen désigne un stylo feutre qui a été conçu pour écrire, dessiner et colorier. Différents feutres rappelent le Sign Pen : 

| Marking Pen | Fountain Penbrush | Pentel Sign Pen | 
| --- | --- |--- |
| crée par Lee W. Newman ( breveté en 1910 ) est composée d'une pointe, d'un tube d'alimentation et d'un réservoir d'encre. ( pour plus d'information sur l'objet : lien https://patents.google.com/patent/US946149A/en )  ![Marking Pen](../images/docs_images_leewnewman.png) | Breveté  en 1926  par Benjamin Paskach, il  est composée d'une pointe en éponge qui absorbe l'encre et qui à son tour rempli le réservoir  ( pour plus d'information sur l'objet : lien  https://patents.google.com/patent/US1601596A/en ) ![Fountain](../images/docs_images_foutainbrushpen.png)| Sa première version officielle du Sign Pen a été crée dans les années 1940. Cependant, le premier stylo à pointe "feutre" est apparu plus tard vers 1963. Dans les années 50, Pentel réside parmis les pionniers des instruments d'écriture modernes. A sa sortie en 1963, le Sign Pen n'est pas très vendu. Il a fallu attendre jusqu'en 1964, pour que Pentel vende près de 800 000 feutres Sign Pen en une année grâce à l'intervention du président Johnson. En 1966, le stylo va faire partie de l'équipement des astronautes du projet spatial Gemini. ![Pentel](../images/docs_images_early-pack.jpg) |

## Design et fabrication

Fabriqués en France dans l’usine de Bry-sur-Marne (94), la seule usine d’Europe.
Encre sans solvants nocifs.

_**Matière**_ : polystyrène cristal/Polyèthylène

Le polystyrène cristal :
Le PS est un thermoplastique transparent avec une structure amorphe. Il est particulièrement utilisé pour la vaisselle jetable, les éléments décoratifs, le boitage économique… 

Point fort                
-  Bonne rigidité mais cassant 
-  Bel aspect brillant         
-  Surface dure de bel aspect  
-  Coût réduit            

Point faible                      
-  Mauvaise tenue température       
-  Cassant                            
-  Très mauvaise résistance chimique 
-  Electrostatique|

Polyèthylène basse densité :
Le PEBD est un thermoplastique souple semi-cristallin. Il est notamment utilisé pour la fabrication de film, de tuyau souple, de jouet, de flacon souple.

Point fort                
-  Meilleure rigidité 
-  Non cassant         
-  Anti-adhérent 
-  Tenue chimique  

Point faible                      
-  Mauvaise tenue température     

_***Procéder de fabrication :***_

![injection](../images/docs_images_proceder_de_fabrication.png)

Injection sous pression de bille de plastique dans un moule en aluminium.
Les billes sont fondu à haute température elle deviennent ensuite liquides.

_***Assemblage :***_

Nombre de pièce :
Trois pièces 
1. Bouchon avec accroche
1. Tube fuseler principale 
1. Bouchon pour fermer le tube fuseler

Légende:
- Vert = Polyèthylène basse densité
- Bleu = Polystyrène cristal

![](../images/schema.png)

## Test et avis 

| Test 1 | Test 2 | Test 3| 
| --- | --- | --- |
|Nous avons testé le feutre sign pen sous différents angles. Celui-ci permet de jouer avec les épaisseurs de traits en fonction de son inclinaison. De cette manière, nous pouvons tracer des lignes relativement fines ainsi que des traits plus marque. Il est important de noter que les différents tests ont été réalisé sur du papier 300g à grains fort. Les traits sont donc peu régulidés et nous pouvons voir que l’écoulement de l’encre n’est parfois pas suffisant.![Test Papier 300g](../images/docs_images_test_papier_300g.png) | En revanche, lorsque le marqueur est utilisé sur une surface plus lisse, plus régulière, ici un support cartonné brillant, les traits sont beaucoup plus précis et les lignes plus régulières. En effet le carton blanc absorbe moins d’encre par rapport au papier 300 grammes. ![Test Carton lisse](../images/docs_images_test_carton_blanc.png) |Nous l’avons également soumis à un test de dessin de perspective. Ce dessin a été réalisé uniquement avec le Sign Pen Uni. Il s’agit donc d’un feutre polyvalent utile pour tracer des lignes franches et à la fois des détails beaucoup plus fins.![Test dessin perspective](../images/docs_images_test_sketch_sign_pen.png) |

De manière générale, nous constatons après test que le sign pen uni est un bon outil de dessin, principalement lorsqu’il s’agit de tracer des lignes et des écritures. Il ne se prête pas aux grands aplats ainsi qu'a toutes les surfaces de dessin en raison du flux d'encre limité, même si cela peut etre un un atout pour un effet de style.