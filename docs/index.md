## A propos de moi

| Coucou | Présentation |
| --- | --- |
|![Cassandra](images/Photop-min.jpg)|<ul><li>Je m'appelle Cassandra Younes, étudiante en master 2 en module 3 de l'option AD. Au cours de mes études j'ai déjà eu l'occasion de suivre le cours de projet Digital Fabrication Studio au Q1 l'année passée et j'ai adoré la pédagogie de cet atelier. *Partage.Discussion.Brainstorming.Technologie*. Cette année nous sommes un peu plus nombreux mais j'espère qu'on pourra continuer à partager nos avis autour de tables rondes et à faire des petits jeux avec Denis le matin pour bien se reveiller. Mis à part la manière d'enseigner qui met de bonne humeur et qui pousse au travail sans la sensation de subir, ce qui est génial au Fablab c'est qu'on a tous des projets très divers et spécifiques et à la fin du quadri on est tous curieux de voir comment les projets ont abouti après avoir suivi les différentes évolutions. On apprend beaucoup des autres et grâce au Git on a accès à tout. <br> <br> <li> Sinon j’adore cuisiner, danser, chanter, la musique et les séries d’animations japonaises.|

## Derniers projets

## Premier quadri 2019

L'an passé lors du Q1 j'ai pu travaillé en binôme avec un étudiant de la VUB en ingénierie industrielle afin de commencer à concevoir une bouée météorologique en se questionnant sur le design d'une part avec les nécéssités du contexte de l'objet et des matériaux à disposition en employant les machines 3D, et du coté éléctronique de l'autre. Ca a été une expérience très intéressante pour moi de travailler avec quelqu'un qui a une manière différente de faire.

Lien du projet: [Bouée météorologique](https://fablab-ulb.gitlab.io/enseignements/2019-2020/fablab-studio/website/final_projects/meteorological-buoy/tutorial/)

## Deuxième quadri 2020

Au second quadri j'ai également continué dans l'univers FabLab mais dû au Covid, nous avons concentré nos efforts sur l'approfondissement de logiciels informatiques paramétriques tels que Rhino/Grasshopper et leurs plug-ins. C'était également très intéressant même si on a du découvrir et apprendre à manier les logiciels en solo. Maintenant c'est un outil en plus que je peux utiliser.

Lien du projet: [Concrete shells](https://projetcass.invisionapp.com/freehand/Jury-final-DFS-Cassandra-Younes-gqLt5RkQo)

## Choix de l'objet

L'objet que j'ai choisi ne passe pas inaperçu de par sa couleur orange flashy et son aspect un peu futuriste inspirés de l'époque pop art. Il ressemble a un oeuf écrasé quand il est fermé et peut se déplier pour se transformer en fauteuil cosy comportant un coussin à l'intérieur. Ce qui m'a le plus intéressé dans cet objet, c'est son aspect transportable et petit comme une valise, il ne fait que un mètre de haut une fois déplié et 43cm fermé. Il peut notamment être utilisé en tant q'assise lorsqu'il est fermé et peut donc être employé sous plusieurs formes. Ce qui m'a aussi plu c'est qu'une fois fermé il resiste à la pluie et peut donc rester en exterieur sans que l'intérieur de l'objet ne soit endommagé.

![Cassandra](images/121785596_412218983115975_523796249878532283_n.jpg)

Garden Egg Chair - Peter Ghyczy